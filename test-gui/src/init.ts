import * as Puppeteer from "puppeteer"

const puppeteer_options = {
    headless: true,
    slowMo: 0
}

before( async function startBrowser () {
    this.browser = await Puppeteer.launch( puppeteer_options )
} )

after( async function stopBrowser () {
    await this.browser.close()
} )
