import * as Puppeteer from "puppeteer"

import { assert } from "chai"

describe( "home page", function () {
    let page: Puppeteer.Page

    beforeEach( "navigate to home page", async function () {
        page = await this.browser.newPage()

        await page.goto( "http://localhost:8080/" )
    } )

    afterEach( async function () {
        await page.close()
    } )

    it( "is accessible", async function () {
        assert.equal( await page.title(), "ACME" )
    } )

    it( "displays default greeting", async function () {
        const message = await page.waitForSelector( "#message" )

        const text = await page.evaluate( function ( message ) {
            return message.textContent
        }, message )

        assert.equal( text, "Hello, World!" )
    } )

    it( "displays a custom greeting", async function () {
        await page.waitForSelector( "form" )
        await page.type( "input[name='name']", "Joe" )
        await page.click( "button" )

        await page.waitFor( function () {
            return document.querySelector( "#message" ).textContent === "Hello, Joe!"
        }, { timeout: 1000 } )
    } )
} )

