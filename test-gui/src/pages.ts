import * as Puppeteer from "puppeteer"

export class Page {
    constructor ( public page: Puppeteer.Page, public path: string ) {
    }

    async goto () {
        await this.page.goto( `http://localhost:8080${ this.path }`, { waitUntil: "load" } )
    }

    async close () {
        await this.page.close()
    }

    async title () {
        return await this.page.title()
    }

    async text ( selector: string ) {
        return await this.page.evaluate( function ( selector ) {
            return document.querySelector( selector ).textContent
        }, selector )
    }
}

export class HomePage extends Page {
    constructor ( public page: Puppeteer.Page ) {
        super( page, "/home.html" )
    }
}