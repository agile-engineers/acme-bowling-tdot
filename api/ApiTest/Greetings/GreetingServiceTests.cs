using ApiWeb.Greetings;
using Xunit;
using ApiWeb.Greetings;
using Moq;

namespace ApiTest.Greetings
{
    public class GreetingServiceTests
    {
        [Fact]
        public void build_a_default_greeting()
        {
            var translationGateway = new Mock<ITranslationGateway>();
            GreetingService sut = new GreetingService(translationGateway.Object);

            translationGateway.Setup(tg => tg.Translate("english")).Returns("Hello");

            Greeting actual = sut.Build();

            Assert.Equal("Hello, World!", actual.Text);
        }

        [Fact]
        public void build_a_name_greeting()
        {
            var translationGateway = new Mock<ITranslationGateway>();
            GreetingService sut = new GreetingService(translationGateway.Object);

            translationGateway.Setup(tg => tg.Translate("english")).Returns("Hello");
            var name = "slosh";
            Greeting actual = sut.Build(name);

            Assert.Equal($"Hello, {name}!", actual.Text);
        }

        [Fact]
        public void build_custom_greeting_with_name_and_language()
        {
            var translationGateway = new Mock<ITranslationGateway>();
            GreetingService sut = new GreetingService(translationGateway.Object);

            translationGateway.Setup(tg => tg.Translate("spanish")).Returns("Hola");

            var result = sut.Build("Joe", "spanish");

            Assert.Equal("Hola, Joe!", result.Text);
        }

        [Fact]
        public void send_custom_greeting_by_email()
        {
            var translationGateway = new Mock<ITranslationGateway>();
            translationGateway.Setup(tg => tg.Translate("english")).Returns("Hello");
            var sut = new GreetingService(translationGateway.Object);
            var emailGateway = new Mock<IEmailGateway>();

            sut.SendMail(emailGateway.Object, "Joe", "joe@mail.com");

            emailGateway.Verify(es =>
                es.Send("joe@mail.com", "Hello, Joe!"));
        }
    }
}