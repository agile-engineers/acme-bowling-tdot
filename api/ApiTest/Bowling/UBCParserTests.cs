﻿using System;
using System.Collections.Generic;
using Xunit;
using ApiWeb.Bowling;

namespace ApiTest.Bowling
{
    public class UBCParserTests
    {
        [Fact]
        public void ParseSingleValue_Gutter_Symbol()
        {
            var currentVal = '-';
            var result = UBCParser.ParseSingleValue(currentVal);
            Assert.Equal(0, result);
        }

        [Fact]
        public void ParseSingleValue_Strike_Symbol()
        {
            char currentVal = 'X';
            var result = UBCParser.ParseSingleValue(currentVal);
            Assert.Equal(10, result);
            currentVal = 'x';
            result = UBCParser.ParseSingleValue(currentVal);
            Assert.Equal(10, result);
        }

        [Fact]
        public void ParseSingleValue_Number_Symbol()
        {
            for (var i = 0; i < 10; i++)
            {
                Assert.Equal(i, UBCParser.ParseSingleValue(i.ToString().ToCharArray()[0]));
            }
        }

        [Fact]
        public void Parse_Should_Parse_A_Valid_UBC_String()
        {
            var ubcString = "-1-2-3-4-5-6-7-8-9-/1/2/3/4/5/6/7/8/9/x";
            var expectedRtn = new List<int> {0,1,0,2,0,3,0,4,0,5,0,6,0,7,0,8,0,9,0,10,1,9,2,8,3,7,4,6,5,5,6,4,7,3,8,2,9,1,10 };
            var rtn = UBCParser.Parse(ubcString);
            Assert.Equal(expectedRtn, rtn);
        }

        [Fact]
        public void Parse_Should_Not_Parse_An_InValid_UBC_String_No_Previous_before_spare()
        {
            var ubcString = "/-";
            var e = Assert.Throws<ApplicationException>(() => UBCParser.Parse(ubcString));
            Assert.Contains("no previous value", e.Message);
        }

        [Fact]
        public void Parse_Should_Not_Parse_An_InValid_UBC_String_With_Invalid_Symbol()
        {
            var ubcString = "0-";
            var e = Assert.Throws<ApplicationException>(()=>UBCParser.Parse(ubcString));
            Assert.Contains("UBC String you passed has an invalid value!", e.Message);
            ubcString = "*-";
            e = Assert.Throws<ApplicationException>(()=>UBCParser.Parse(ubcString));
            Assert.Contains("UBC String you passed has an invalid value!", e.Message);
        }
    }
}
