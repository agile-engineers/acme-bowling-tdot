﻿using System.Collections.Generic;
using Xunit;
using ApiWeb.Bowling;
using System;

namespace ApiTest.Bowling
{
    public class CreateScoreTest
    {
        [Fact]
        public void CreateScore_intArray_ForAll_Gitter()
        {
            var val = new List<int> { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            var rtn = UBCScorer.CreateScore(val);
            Assert.Equal(0, rtn);
        }

        [Fact]
        public void CreateScore_intArray_ForAll_Ones()
        {
            var val = new List<int> { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
            var rtn = UBCScorer.CreateScore(val);
            Assert.Equal(20, rtn);
        }

        [Fact]
        public void CreateScore_intArray_ForAll_Twos()
        {
            var val = new List<int> { 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 };
            var rtn = UBCScorer.CreateScore(val);
            Assert.Equal(40, rtn);
        }

        [Fact]
        public void CreateScore_intArray_ForSpare_WithTwos_at_end()
        {
            var val = new List<int> { 0, 0, 2, 8, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 8, 2};
            var rtn = UBCScorer.CreateScore(val);
            Assert.Equal(26, rtn);
        }

        [Fact]
        public void CreateScore_intArray_ForAllSpares()
        {
            var val = new List<int> { 2, 8, 2, 8, 2, 8, 2, 8, 2, 8, 2, 8, 2, 8, 2, 8, 2, 8, 2, 8, 2};
            var rtn = UBCScorer.CreateScore(val);
            Assert.Equal(120, rtn);
        }

        [Fact]
        public void CreateScore_intArray_ForSpare_WithTwos()
        {
            var val = new List<int> { 2, 8, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 };
            var rtn = UBCScorer.CreateScore(val);
            Assert.Equal(48, rtn);
        }

        [Fact]
        public void CreateScore_intArray_ForStrike_WithTwos()
        {
            var val = new List<int> { 2, 2, 10, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            var rtn = UBCScorer.CreateScore(val);
            Assert.Equal(22, rtn);
        }

        [Fact]
        public void CreateScore_intArray_ForStrike_WithTwos_At_End()
        {
            var val = new List<int> { 2, 2, 10, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 10, 10 };
            var rtn = UBCScorer.CreateScore(val);
            Assert.Equal(52, rtn);
        }

        [Fact]
        public void CreateScore_intArray_ForAllStrikes()
        {
            var val = new List<int> { 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10 };
            var rtn = UBCScorer.CreateScore(val);
            Assert.Equal(300, rtn);
        }

        [Fact]
        public void CreateScore_string_ForAllStrikes()
        {
            var val = "xxxxxXXXXXxx";
            var rtn = UBCScorer.CreateScore(val);
            Assert.Equal(300, rtn);
        }

        [Fact]
        public void CreateScore_string_ForAllOnes()
        {
            var val = "11111111111111111111";
            var rtn = UBCScorer.CreateScore(val);
            Assert.Equal(20, rtn);
        }

        [Fact]
        public void CreateScore_string_ForAllTwos()
        {
            var val = "22222222222222222222";
            var rtn = UBCScorer.CreateScore(val);
            Assert.Equal(40, rtn);
        }

        [Fact]
        public void CreateScore_string_ForAllTwos_with_spare()
        {
            var val = "22282222222222222222";
            var rtn = UBCScorer.CreateScore(val);
            Assert.Equal(48, rtn);
        }

        [Fact]
        public void CreateScore_string_ForAllTwos_with_spare_at_end()
        {
            var val = "222/222222222222222/2";
            var rtn = UBCScorer.CreateScore(val);
            Assert.Equal(56, rtn);
        }

        [Fact]
        public void CreateScore_string_ForAllTwos_with_strike_at_end()
        {
            var val = "--282-------------Xxx";
            var rtn = UBCScorer.CreateScore(val);
            Assert.Equal(44, rtn);
        }

        [Fact]
        public void CreateScore_string_ForStrange_string()
        {
            var val = "24135/xx--3/121211";
            var rtn = UBCScorer.CreateScore(val);
            Assert.Equal(79, rtn);
        }

        [Fact]
        public void CreateScore_string_ForInvalid_string_throw_error()
        {
            var val = "24135/xx--3/121211!";
            var e =Assert.Throws<ApplicationException>(()=> UBCScorer.CreateScore(val));
            Assert.Contains("invalid UBS score", e.Message);
            val = "24135/xx--3/1212";
            e =Assert.Throws<ApplicationException>(()=> UBCScorer.CreateScore(val));
            Assert.Contains("invalid UBS score", e.Message);
        }
    }
}
