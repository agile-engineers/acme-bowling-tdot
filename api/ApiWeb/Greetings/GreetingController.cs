using Microsoft.AspNetCore.Mvc;

namespace ApiWeb.Greetings
{
    [Route("queries/greeting")]
    public class GreetingController : Controller
    {
        [HttpGet]
        public IActionResult Get()
        {
            ITranslationGateway translationGateway = null;
            var service = new GreetingService(translationGateway);

            return Ok(service.Build());
        }
    }
}