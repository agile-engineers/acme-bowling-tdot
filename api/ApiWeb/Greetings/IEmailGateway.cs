﻿namespace ApiWeb.Greetings
{
    public interface IEmailGateway
    {
        void Send(string emailAddress, string greeting);
    }
}
