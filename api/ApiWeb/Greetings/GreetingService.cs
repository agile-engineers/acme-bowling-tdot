namespace ApiWeb.Greetings
{
    public class GreetingService
    {
        private ITranslationGateway _translationGateway;
        public GreetingService(ITranslationGateway translationGateway)
        {
            _translationGateway = translationGateway;
        }

        public Greeting Build(string name = "World", string language = "english")
        {
            var helloString = _translationGateway.Translate(language);
            return new Greeting { Text = $"{helloString}, {name}!" };
        }

        public void SendMail(IEmailGateway emailGateway, string name, string emailAddress)
        {
            emailGateway.Send(emailAddress, Build(name).Text);
        }
   }
}