﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace ApiWeb
{
    public class Program
    {   //Slosh
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}