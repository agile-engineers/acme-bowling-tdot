﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace ApiWeb.Bowling
{
    public class UBCScorer
    {
        public static int CreateScore(IList<int> frameArray)
        {
            int totalScore = 0, ballOneIndex = 0, frameScore;
            for (var frame = 0; frame < 10; frame++)
            {   //We only have ten frames...so we will get the balls associated with them
                //and calculate the score based on that...Each frame should have two balls except for
                //Strikes and the "last" extra frame
                //Get the value for the first ball
                frameScore = frameArray[ballOneIndex];
                //see if we have a strike
                if (frameScore == 10)
                {   //Now add the two bonus values to the frame
                    frameScore += frameArray[ballOneIndex + 1];
                    frameScore += frameArray[ballOneIndex + 2];
                    ballOneIndex++;
                }
                else
                {   //Since it was not a strike, add the second ball
                    frameScore += frameArray[ballOneIndex + 1];
                    //See if we have a spare, add the bonus
                    if (frameScore == 10) frameScore += frameArray[ballOneIndex + 2];
                    ballOneIndex += 2;
                }
                //Now, add frame score to total
                totalScore += frameScore;
            }
            return totalScore;
        }

        public static int CreateScore(string frameArray)
        {   //We need to check to ensure that the "string" is a valid game...
            //has ten frames...plus bonuses if necessary
            var regex = new Regex(@"^([xX]|[1-9\-][1-9-\/]){9}([xX][1-9xX\-][1-9xX\-\/]|[1-9\-]\/[1-9\-xX]|[1-9\-]{2})$");
            if (!regex.IsMatch(frameArray)) throw new ApplicationException("You have passed an invalid UBS score string!");
            var ubcScoreIntList = UBCParser.Parse(frameArray);
            return CreateScore(ubcScoreIntList);
        }
    }
}
