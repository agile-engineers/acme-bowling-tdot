﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
namespace ApiWeb.Bowling
{
    public class UBCParser
    {
        public static IList<int> Parse(string value)
        {   //First check to ensure the string is valid
            var regx = new Regex(@"^[1-9\-xX\/]+$");
            if(!regx.IsMatch(value)) throw new ApplicationException("UBC String you passed has an invalid value!");
            IList<int> rtn = new List<int>();
            //Now split the string into individual parts
            char previousValue = ' ';
            foreach (var currentValue in value)
            {
                if (currentValue.Equals('/'))
                {   //Because we have a spare, we need to parse the previous value (if it exists)
                    if (string.IsNullOrWhiteSpace(previousValue.ToString())) throw new ApplicationException("There was no previous value for the spare!");
                    rtn.Add(10 - ParseSingleValue(previousValue));
                }
                else
                { rtn.Add(ParseSingleValue(currentValue)); }
                
                previousValue = currentValue;
            }
            return rtn;
        }

        public static int ParseSingleValue(char currentValue)
        {
            if (currentValue.ToString().Equals("X", StringComparison.InvariantCultureIgnoreCase)) return 10;
            if (currentValue.Equals('-')) return 0;
            return int.Parse(currentValue.ToString());
        }
    }
}
