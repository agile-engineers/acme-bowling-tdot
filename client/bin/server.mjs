import Path from "path"
import Express from "express"
import Browserify from "browserify"

const app = Express()

app.get( "*/index.js", function ( req, res ) {
    const browserify = Browserify( { debug: true, standalone: "index" } )

    const index_script_filename = Path.join( "build", "development", req.url )
    const index_script_path = Path.resolve( index_script_filename )

    console.log( `Bundle: ${ index_script_filename }` )

    browserify.external( [] )
    browserify.add( index_script_path )

    browserify.bundle( function ( error, buffer ) {
        if ( error ) {
            res.status( 500 ).send( error )
        } else {
            res.set( "content-type", "application/javascript" )
            res.send( buffer )
        }
    } )
} )

app.use( Express.static( Path.join( "build", "development" ) ) )
app.use( Express.static( "static", { index: "home.html" } ) )

app.listen( 8080, function () {
    console.log( "server started" )
} )
