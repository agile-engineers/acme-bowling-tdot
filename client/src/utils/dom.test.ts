import { assert } from "chai"
import { JSDOM } from "jsdom"

import * as DOM from "./dom"

describe( "form extractor", function () {

    const createForm = function ( form_template: string ) {
        return new JSDOM( `<!DOCTYPE html>
            <head><title></title></head>
            <body>${ form_template }</body>
        ` ).window.document.body.childNodes[ 0 ] as HTMLFormElement
    }

    it( "simple text fields", function () {
        const data = DOM.extractFormData( createForm( `<form>
            <input type="text" name="email" />
        </form>` ) )

        assert.deepEqual( data, { email: "" } )
    } )

    it( "radio options defaults to null", function () {
        const data = DOM.extractFormData( createForm( `<form>
            <input type="radio" name="color" value="blue" />
            <input type="radio" name="color" value="green" />
        </form>` ) )

        assert.deepEqual( data, { color: null } )
    } )

    it( "radio options with checked attribute", function () {
        const data = DOM.extractFormData( createForm( `<form>
            <input type="radio" name="color" value="blue" checked />
            <input type="radio" name="color" value="green" />
        </form>` ) )

        assert.deepEqual( data, { color: "blue" } )
    } )

    it( "simple drop down menu", function () {
        const data = DOM.extractFormData( createForm( `<form>
            <select name="color">
                <option value="blue" checked>Blue</option>
                <option value="green">Green</option>
            </select>
        </form>` ) )

        assert.deepEqual( data, { color: "blue" } )
    } )

    it( "simple checkbox", function () {
        const data = DOM.extractFormData( createForm( `<form>
            <input type="checkbox" name="item1" checked />
            <input type="checkbox" name="item2" />
        </form>` ) )

        assert.deepEqual( data, { item1: true, item2: false } )
    } )
} )