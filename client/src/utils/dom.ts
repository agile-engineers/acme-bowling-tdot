export const extractFormData = function ( form: HTMLFormElement ) {
    const fields: any = {}
    const elements: any = form.elements

    for ( let i = 0; i < elements.length; i++ ) {
        const element = elements[ i ]
        if ( element.type === "text" || element.type === "password" || element.type === "textarea" ) {
            fields[ element.name ] = element.value
        } else if ( element.type === "radio" && fields[ element.name ] == undefined ) {
            fields[ element.name ] = element.checked ? element.value : null
        } else if ( element.type === "checkbox" ) {
            fields[ element.name ] = element.checked
        } else if ( element.nodeName === "SELECT" ) {
            fields[ element.name ] = element.value
        } else if ( element.nodeName === "hidden" ) {
            fields[ element.name ] = element.value
        }
    }

    return fields
}