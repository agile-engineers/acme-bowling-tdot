import { Fetcher } from "./types"

const createFetchOptions = function ( method: string, token: string, data?: any, ): RequestInit {
    const headers: any = {
        "content-type": "application/json",
        "accept": "application/json"
    }

    if ( token ) {
        headers[ "Authorization" ] = `Bearer ${ token }`
    }

    const body = data ? JSON.stringify( data ) : null

    return { method, headers, body }
}

export default class HTTP {
    constructor ( private fetcher: Fetcher, private base: string ) {
    }

    fetch ( path: string, options: RequestInit ) {
        return this.fetcher.fetch( path, options )
            .then( function ( response ) {
                if ( response.ok ) {
                    const content_type = response.headers.get( "content-type" )

                    if ( content_type && content_type.includes( "application/json" ) )
                        return response.json()
                    else {
                        throw new Error( "Server did not provide a json content type" )
                    }
                } else {
                    return { error: response.statusText }
                }
            } )
    }

    post ( path: string, data: any, token?: string ) {
        return this.fetch( this.base + path, createFetchOptions( "POST", token, data ) )
    }

    get ( path: string, token?: string ): Promise<any> {
        return this.fetch( this.base + path, createFetchOptions( "GET", token ) )
    }
}
