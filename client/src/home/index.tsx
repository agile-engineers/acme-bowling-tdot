import HTTP from "../utils/http"
import { h, app } from "hyperapp"

import * as DOM from "../utils/dom"

const createState = function () {
    return {
        message: "",
        valid: false
    }
}

const createActions = function ( http: HTTP ) {
    return {
        updateMessage: greeting => function ( state ) {
            return { ...state, message: greeting.text }
        },
        fetchDefaultMessage: () => function ( state, actions ) {
            return http.get( "/queries/greeting" ).then( function ( data ) {
                actions.updateMessage( data )
            } )
        },
        fetchCustomMessage: event => function ( state, actions ) {
            const data = DOM.extractFormData( event.currentTarget )
            return http.get( `/queries/greeting/${ data.name }` )
                .then( actions.updateMessage )
        },
        validateForm: event => function ( state ) {
            const data = DOM.extractFormData( event.currentTarget )
            return { ...state, valid: data.name.length > 0 }
        }
    }
}

const view = function ( state, actions ) {
    const waitingView = <section>
        <h2>Waiting..</h2>
    </section>

    const formView = <section>
        <h2 id="message">{ state.message }</h2>
        <form onsubmit={ actions.fetchCustomMessage }
              oninput={ actions.validateForm }>
            <input type="text" name="name" autocomplete="off"/>
            <button disabled={ !state.valid }>Request Greeting</button>
        </form>
        <p>Made with Love!</p>
    </section>

    return <main oncreate={ actions.fetchDefaultMessage }>
        { state.message === "" ? waitingView : formView }
    </main>
}

export const start = function ( window: Window ) {
    const http = new HTTP( window, "http://localhost:8081" )
    const actions = createActions( http )
    const state = createState()

    window.addEventListener( "submit", e => e.preventDefault() )

    app( state, actions, view, window.document.body )
}

