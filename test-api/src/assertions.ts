import { assert } from "chai"

export const responseIncludes = function ( expected ) {
    return function ( response ) {
        assert.equal( response.status, 200 )
        assert.deepInclude( response.body, expected )
    }
}

export const responseEquals = function ( expected ) {
    return function ( response ) {
        assert.equal( response.status, 200 )
        assert.deepEqual( response.body, expected )
    }
}

export const failsWith = function ( expected_status: number ) {
    return function ( response ) {
        assert.equal( response.status, expected_status )
    }
}
