import { HTTPClient } from "./utils"
import { responseEquals } from "./assertions"

describe( "greeting", function () {
    const http = HTTPClient.withBaseURL( "http://localhost:8081" )

    it( "returns default greeting", function () {
        return http.get( "/queries/greeting" )
            .then( responseEquals( { text: "Hello, World!" } ) )
    } )

    it( "returns custom greeting by name", function () {
        return http.get( "/queries/greeting/Joe" )
            .then( responseEquals( { text: "Hello, Joe!" } ) )
    } )
} )
