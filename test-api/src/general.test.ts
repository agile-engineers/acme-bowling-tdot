import { HTTPClient } from "./utils"
import { failsWith } from "./assertions"

describe( "general", function () {
    const http = HTTPClient.withBaseURL( "http://localhost:8081" )

    it( "returns error response for unknown paths", function () {
        return http.get( "/unknown" ).then( failsWith( 404 ) )
    } )

} )
